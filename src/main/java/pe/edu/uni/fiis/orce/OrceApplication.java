package pe.edu.uni.fiis.orce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrceApplication.class, args);
	}

}
