package pe.edu.uni.fiis.orce.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/foo")
public class FooController {
    @GetMapping
    private String getFoo(){
        return "rand";
    }
}
